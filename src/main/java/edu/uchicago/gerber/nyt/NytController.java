/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber.nyt;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class NytController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
       
        
     //   JsonParser jsonParser = new JsonParser()
      String strUrl =   "http://api.nytimes.com/svc/movies/v2/reviews/search.json?query=big&thousand-best=Y&api-key=6aac490155e332f996fc982556631835:14:69615440";
     // JSONObject jsonObject =  new JSONParser().getJSONFromUrl(strUrl);
      NytSearchResults nytResult  = new Gson().fromJson(getJSON(strUrl, 0), NytSearchResults.class);
   
        System.out.println(nytResult);
        
        
    }    
    
    public String getJSON(String url, int timeout) {
    try {
        URL u = new URL(url);
        HttpURLConnection c = (HttpURLConnection) u.openConnection();
        c.setRequestMethod("GET");
        c.setRequestProperty("Content-length", "0");
        c.setUseCaches(false);
        c.setAllowUserInteraction(false);
        c.setConnectTimeout(timeout);
        c.setReadTimeout(timeout);
        c.connect();
        int status = c.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                return sb.toString();
        }

    } catch (MalformedURLException ex) {
       // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
       // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
}
    
}
